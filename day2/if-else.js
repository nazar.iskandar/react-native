// Soal If-Else dan switch
// IF-ELSE	
function namaPeran(name, role){
	var hasil = "";
	if (name == "") {
		hasil = "Nama harus diisi!";
	} else if ((name != "") && (role == "")) {
		hasil = "Halo " + name + ", Pilih peranmu untuk memulai game!";
	} else if ((name != "") && (role == "Guard")) {
		hasil = "Selamat datang di Dunia Werewolf, " + name + "\nHalo Guard " + name + ", kamu akan membantu melindungi temanmu dari serangan werewolf.";
	}else if ((name != "") && (role == "Werewolf")) {
		hasil = "Selamat datang di Dunia Werewolf, " + name + "\nHalo Werewolf " + name + ", Kamu akan memakan mangsa setiap malam!";
	}else if ((name != "") && (role == "Penyihir")) {
		hasil = "Selamat datang di Dunia Werewolf, " + name + "\nHalo Penyihir " + name + ", kamu dapat melihat siapa yang menjadi werewolf!";
	}
	return hasil;
}
var nama = "John";
var peran = "";
console.log(namaPeran(nama, peran));
var nama = "Jane";
var peran = "Penyihir";
console.log(namaPeran(nama, peran));
var nama = "Jenita";
var peran = "Guard";
console.log(namaPeran(nama, peran));
var nama = "Junaedi";
var peran = "Werewolf";
console.log(namaPeran(nama, peran));
console.log('-------------------'); 

// SWITCH
var hari = 8; 
var bulan = 12; 
var tahun = 1945;

switch (true) {
    case (hari<1):{
        console.log("Input Hari Salah");
		break;}
    case (hari > 31):{
        console.log("Input Hari Salah");
		break;}
	case (hari >= 1) && (hari <= 31):{
        console.log(hari);
        break;}
}

var arrayBulan = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
switch (true) {
    case (bulan<1):{
        console.log("Input Bulan Salah");
		break;}
    case (bulan > 12):{
        console.log("Input Bulan Salah");
		break;}
	case (bulan >= 1) && (bulan <= 12):{
        console.log(arrayBulan[bulan]);
        break;}
}

switch (true) {
    case (tahun<1900):{
        console.log("Input Tahun Salah");
		break;}
    case (tahun > 2200):{
        console.log("Input Bulan Salah");
		break;}
	case (tahun >= 1900) && (tahun <= 2200):{
        console.log(tahun);
        break;}
}


