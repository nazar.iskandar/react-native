// soal no 1

function arrayToObject(arr) {
    // Code di sini 
	var now = new Date()
	var thisYear = now.getFullYear(); // 2020 (tahun sekarang)
	if(arr.length > 0){
		for(var i = 0; i < arr.length; i++){
			if(arr[i][3] > thisYear){
				usia = "Invalid Birth Year";
			}else if(arr[i][3] != null){
				var usia = (thisYear - arr[i][3]);
			}else usia = "Invalid Birth Year";
			var obj = {
				firstName: arr[i][0], lastName: arr[i][1], gender: arr[i][2], age: usia
			};
			console.log((i + 1) + ". " + arr[i][0] + " " + arr[i][1] + " :", obj);
		}
	}else{
		console.log(); // kosong
	}
	
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people) ;
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2);
// Error case 
arrayToObject([]) // ""
console.log("---------------------------------------");

// soal no 2
arrayBarang = [["Sepatu Stacattu", 1500000], ['Baru Zoro', 500000], ['Baju H&N', 250000], ['Sweater Uniklooh', 175000], ['Casing Handphone', 50000]];

function listPurchase(money){
	var sisaMoney = money;
	var purchased = [];
	for(var i = 0; i < arrayBarang.length; i++){
		if(sisaMoney >= arrayBarang[i][1]){
			purchased.push(arrayBarang[i][0]);
			sisaMoney -= arrayBarang[i][1];
		}
	}
	var hasil = [purchased, sisaMoney];
	return hasil;
}
// console.log(listPurchase(1000000)[0]);
function shoppingTime(memberId, money) {
  // you can only write your code here!
	if(memberId != null && memberId != ''){
		if(money < 50000){
			return "Mohon maaf, uang tidak cukup";
		}else{
			var purchase = listPurchase(money);
			var objek = {
				memberId : memberId, money : money , listPurchased: purchase[0], changeMoney: purchase[1]
			}
			return objek;
		}
		
	}else{
		return "Mohon maaf, toko X hanya berlaku untuk member saja";
	}
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("---------------------------------------");
// soal no 3
rute = ['A', 'B', 'C', 'D', 'E', 'F'];
function searchRute(arrayRute, ruteSearchTerm){
	var hasil = 0;
	for(var i = 0; i < arrayRute.length; i++){
		if(arrayRute[i] == ruteSearchTerm){
			hasil = i;
		}
	}
	return hasil;
};

function naikAngkot(arrPenumpang) {
	//your code here
	var obj = [];
	if(arrPenumpang.length > 0){
		for(var i = 0; i < arrPenumpang.length; i++){
			var objAngkot = {
				penumpang: arrPenumpang[i][0], naikDari: arrPenumpang[i][1], tujuan: arrPenumpang[i][2], bayar: 2000 * (searchRute(rute, arrPenumpang[i][2]) - searchRute(rute, arrPenumpang[i][1]))
			};
			obj[i] = objAngkot;
		}
		return obj;
	}else{
		return ""; // kosong
	}
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]

