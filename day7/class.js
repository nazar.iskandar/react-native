// release 0

class Animal {
    // Code class di sini
	constructor(name) {
        this.name = name
        this.legs = 4
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log("------------------------");
// rea
class Ape extends Animal {
    // Code class di sini
	constructor(name) {
        super(name);
        this.legs = 2
    }
	
	yell(){
		console.log("Auooo");
	}
}

class Frog extends Animal {
    // Code class di sini
	constructor(name) {
        super(name);
    }
	
	jump(){
		console.log("hop hop");
	}
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
console.log("------------------------");
// function to class
class Clock {
    // Code di sini

	constructor(template) {
        this.timer = template.template;
    }
	render(){
		var date = new Date();

		var hours = date.getHours();
			if (hours < 10) hours = '0' + hours;

		var mins = date.getMinutes();
			if (mins < 10) mins = '0' + mins;

		var secs = date.getSeconds();
			if (secs < 10) secs = '0' + secs;

		var output = this.timer
		  .replace('h', hours)
		  .replace('m', mins)
		  .replace('s', secs);

		console.log(output);
	}
	stop(){
		clearInterval(timer);
	}
	start(){
		this.render();
		this.timer = setInterval(this.render, 1000);
	}
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  