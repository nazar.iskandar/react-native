// soal no 1
function loopingWhile() { // function untuk soal no 1 looping pakai while
	var hasil = "LOOPING PERTAMA<br>";
	var i = 1;
	while (i <= 20) {
		if (i % 2 == 0) {
			hasil += i + " - I love coding\n";
		}
		i++;
	}
	hasil += "LOOPING KEDUA\n";
	var j = 20;
	while (j > 1) {
		if (j % 2 == 0) {
			hasil += j + " - I will become web developer\n";
		}
		j--;
	}
	return hasil;
}

var jawaban1 = loopingWhile();
console.log(jawaban1);
console.log("------------------------------");

// soal no 2
function loopingFor() { // function untuk soal no 2 looping pakai for
    var hasil = "";
	for (var i = 1; i <= 20; i++) {
		if ((i % 2 == 1) && (i % 3 == 0)) { // nilai ganjil dan kelipatan 3
			hasil += i + " - I Love Coding\n";
		} else if (i % 2 == 1) { // nilai ganjil
			hasil += i + " - Santai\n";
		} else if (i % 2 == 0) { // nilai genap
			hasil += i + " - Berkualitas\n";
		}
	}
	return hasil;
}
var jawaban2 = loopingFor();
console.log(jawaban2);
console.log("------------------------------");

// soal no 3

function loopingPersegi(){
	var hasil = "";
	for(var i = 0; i < 4; i ++){
		hasil += "########\n";
	}
	return hasil;
}
var jawaban3 = loopingPersegi();
console.log(jawaban3);
console.log("------------------------------");

// soal no 4
function loopingTangga(){
	var hasil = "";
	for(var i = 1; i <= 7; i ++){
		for(var j = 0; j < i; j++){
			hasil += "#";
		}
		hasil += "\n";
		
	}
	return hasil;
}
var jawaban4 = loopingTangga();
console.log(jawaban4);
console.log("------------------------------");

// soal no 5
function papanCatur(){
	var hasil = "";
	for(var i = 0; i < 8; i ++){
		if(i % 2 == 0){
			for(var j = 0; j < 8; j++){
				if(j % 2 == 0){
					hasil += " ";
				}else if(j % 2 == 1){
					hasil += "#";
				}
			}
		}else if(i % 2 == 1){
			for(var j = 0; j < 8; j++){
				if(j % 2 == 0){
					hasil += "#";
				}else if(j % 2 == 1){
					hasil += " ";
				}
			}
		}
		
		hasil += "\n";
		
	}
	return hasil;
}

var jawaban5 = papanCatur();
console.log(jawaban5);
console.log("------------------------------");


